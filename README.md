# README #

![Build status](https://raphaelbickel.visualstudio.com/_apis/public/build/definitions/dfcb2c0d-0dee-45e6-b6b2-d95daac86af9/1/badge)

### What is this repository for? ###

* JwtBearer Firebase authentication middleware for AspNetCore 2.0 (and higher)
* For rpevious version, pelase download 1.* plugin version

### Setup a Firebase project ###

* Create a Firebase project. https://console.firebase.google.com/

**Issuer** will be *https://securetoken.google.com/MYPROJECTNAME* and **Audience** will be *MYPROJECTNAME*

Enable authentication provider (Google, Facebook, Anonymous, OTP, ...) on your project.

### Use AspNetCore.Firebase.Authentication in AspNetCore >= 2.0 web application ###

```
		 Install-Package AspNetCore.Firebase.Authentication
```
https://www.nuget.org/packages/AspNetCore.Firebase.Authentication/


* In Startup.cs -> 

```csharp
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddFirebaseAuthentication(Configuration["FirebaseAuthentication:Issuer"], Configuration["FirebaseAuthentication:Audience"]);
		}
```


```csharp
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			app.UseAuthentication();
		}
```

* Just have to use [Authorize] attribute on your controllers to enforce authorization