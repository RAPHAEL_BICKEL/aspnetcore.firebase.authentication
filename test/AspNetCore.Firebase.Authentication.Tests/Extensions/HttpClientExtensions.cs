﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCore.Firebase.Authentication.Tests.Extensions
{
    public static class HttpClientExtensions
    {
	    public static Task<string> GetStringAsyncWithToken(this HttpClient client, string uri, string token)
	    {
		    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
		    return client.GetStringAsync(uri);
	    }

	    public static Task<HttpResponseMessage> GetAsyncWithToken(this HttpClient client, string uri, string token)
	    {
		    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
		    return client.GetAsync(uri);
	    }
	}
}
