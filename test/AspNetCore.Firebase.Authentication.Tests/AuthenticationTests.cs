﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AspNetCore.Firebase.Authentication.Tests.Extensions;
using Xunit;

namespace AspNetCore.Firebase.Authentication.Tests
{
    public class AuthenticationTests : IClassFixture<AuthenticationFixture>, IDisposable
    {
	    private readonly AuthenticationFixture _fixture;
		public AuthenticationTests(AuthenticationFixture fixture)
	    {

		    _fixture = fixture;
	    }

		[Fact(DisplayName = nameof(TestAuthenticationWithAnonymousToken))]
	    public async Task TestAuthenticationWithAnonymousToken()
	    {

		    using (var client = _fixture.GetTestClient())
		    {
			    var anonymousToken = await _fixture.GetAnonymousToken();
			    var response = await client.GetAsyncWithToken("/anyurl", anonymousToken);
				Assert.Equal(HttpStatusCode.OK, response.StatusCode);

		    }
	    }

		[Fact(DisplayName = nameof(TestAuthenticationWithNoToken))]
		public async Task TestAuthenticationWithNoToken()
	    {

		    using (var client = _fixture.GetTestClient())
		    {
			    var response = await client.GetAsyncWithToken("/anyurl", "");
			    Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
		    }
	    }

		[Fact(DisplayName = nameof(TestAuthenticationWithWrongIssuerAndAudienceToken))]
		public async Task TestAuthenticationWithWrongIssuerAndAudienceToken()
	    {

		    using (var client = _fixture.GetTestClient())
		    {
			    var response = await client.GetAsyncWithToken("/anyurl", "eyJhbGciOiJSUzI1NiIsImtpZCI6ImMzZDY0Y2YzMDQwMjM3ZDc2NzFmMTE1MjIxNmM5Yzk0NmMyOTBiZGEifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYml6enktZGV2IiwicHJvdmlkZXJfaWQiOiJhbm9ueW1vdXMiLCJhdWQiOiJiaXp6eS1kZXYiLCJhdXRoX3RpbWUiOjE1MDUxOTE0NTUsInVzZXJfaWQiOiJzTUI3WjdTWEY0UkhISks5ejFVU2RpMlRFQzUzIiwic3ViIjoic01CN1o3U1hGNFJISEpLOXoxVVNkaTJURUM1MyIsImlhdCI6MTUwNTE5MTQ1NiwiZXhwIjoxNTA1MTk1MDU2LCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7fSwic2lnbl9pbl9wcm92aWRlciI6ImFub255bW91cyJ9fQ.sbuhteoPk_34iEV5SM70GOP8SOHdhqQGtfIFxMtoig_vK020h4kybMA0nz5tq55PWqiDrl5dnYGNd-nHkPlvI7pl9syqinNlFrLHBHjF6X-7e9jPSwySNhvZhPIhwcC-0H8P8BvCo4C4KWcDvFV8NKv3ha365w471bQaCUP-z7XkB5rMeUKmk147WfDr_ozQEvQhEYUZ4DipaOw9L66ey2yLLFM5Jwp7627vrtj-lCL43Kk7OPIl_gyyOC3l3VFfoWPuhKD3xAei8LzBwUUAiplJ0VqMl9Ez5mccTwAzeY0Xe28Fn_bD0rKc8AWIUF7RGVv0hwbGbruF1bpCLvaZdA");
			    Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
		    }
	    }

		[Fact(DisplayName = nameof(TestAuthenticationWithExpiredToken))]
		public async Task TestAuthenticationWithExpiredToken()
	    {

		    using (var client = _fixture.GetTestClient())
		    {
			    var response = await client.GetAsyncWithToken("/anyurl", "eyJhbGciOiJSUzI1NiIsImtpZCI6ImE0YTA5YThlZGU1YmI2NTNhZWY4MGE2M2UzMmMxY2Y3MWNiMWI3ODEifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYXNwbmV0Y29yZWZpcmViYXNlYXV0aCIsInByb3ZpZGVyX2lkIjoiYW5vbnltb3VzIiwiYXVkIjoiYXNwbmV0Y29yZWZpcmViYXNlYXV0aCIsImF1dGhfdGltZSI6MTUwNTE5MTUzOSwidXNlcl9pZCI6IlpKQlFDV3ViQXBmY3BhUEkzZ21naEFjdHpsTDIiLCJzdWIiOiJaSkJRQ1d1YkFwZmNwYVBJM2dtZ2hBY3R6bEwyIiwiaWF0IjoxNTA1MTkxNTM5LCJleHAiOjE1MDUxOTUxMzksImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnt9LCJzaWduX2luX3Byb3ZpZGVyIjoiYW5vbnltb3VzIn19.KCqJStkfNo29-n1z5bk7CqVT2x7iVuOXRbSP6CZaaIAQFzZfx1c8Whz_BtaM98eocYSnKG8x_-JcSeZcv3HLzLtUmiJIIXHWKh6GnTLZfGy_4La7IZDbTMeTmOmzVd1zL6IWjqBsj45Rme4Gn-qYTonw5kiPdlGfyJSvXzwoCb8A4LV-fUwe3tAxuoyuAkNggMcdtuSpvW_pmq0KVjLBop0oi9RjLWpUHtkoq6gpyg1ixVFSN6lzQOhl1CQWv5av5IuBN4ln8BCIIeJHdOyzj9FkoXylcfECjPR1ArijIbEXkcDpaZodMDA4D5HjfMLhl3vbjW_bboMi-ICxUDbTAw");
			    Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
		    }
	    }

		[Fact(DisplayName = nameof(TestAuthenticationWithModifiedToken))]
		public async Task TestAuthenticationWithModifiedToken()
	    {
		    var anonymousToken = await _fixture.GetAnonymousToken();
		    char replacement = anonymousToken[30] == 'A' ? 'B' : 'A';
		    anonymousToken = anonymousToken.Remove(30).Insert(30, replacement.ToString());

			using (var client = _fixture.GetTestClient())
		    {
			    var response = await client.GetAsyncWithToken("/anyurl", anonymousToken);
			    Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
		    }
	    }


		public void Dispose()
	    {
	    }
    }
}
