﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AspNetCore.Firebase.Authentication.Tests
{
	public class AuthenticationFixture : IDisposable
	{
		public IConfigurationRoot Configuration { get; }

		public AuthenticationFixture()
		{
			var configurationBuilder = new ConfigurationBuilder()
				.SetBasePath(AppContext.BaseDirectory)
				.AddJsonFile($"appsettings.testing.json", optional: true);


			Configuration = configurationBuilder.Build();
		}

		public HttpClient GetTestClient()
		{
			var builder = new WebHostBuilder()
				.UseContentRoot(AppContext.BaseDirectory)
				.ConfigureLogging(lf =>
				{
					lf.AddConsole();
				})
				.UseStartup<TestServerStartup>()
				.UseEnvironment("Testing");  // ensure ConfigureTesting is called in Startup

			var server = new TestServer(builder);
			var client = server.CreateClient();
			// client always expects json results
			client.DefaultRequestHeaders.Clear();
			client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));
			return client;
		}

		public async Task<string> GetAnonymousToken()
		{
			using (var client = new HttpClient())
			{
				var apiKey = Configuration["FirebaseAuthentication:ApiKey"];
				var response = await client.PostAsync($"https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key={apiKey}", null);
				return JsonConvert.DeserializeObject<dynamic>(await response.Content.ReadAsStringAsync()).idToken;
			}
		}

		public void Dispose()
		{
		}
	}
}
